# Groupe n°17

## "Je suis sapé chez So"

### Composition de l'équipe



| Nom                     | Prénom                   | Email                             |
| -------------           |-------------             |-------------                      |
| HARTMANN                | Kentigern                | kentigern.hartmann@ensiie.fr      |
| EL ATTAOUI              | Mounia                   | attaouimounia@gmail.com           |
| KIHAL                   | Sofiane                  | isoft.k@gmail.com                 |
| BELLA BACI              | Ali                      | ali.bellabaci@gmail.com           |
| ZEGHIDA                 | Amina                    | zeghida.amina@gmail.com           |
| BENELMOSTAFA            | Sami                     | samibenelmostafa@gmail.com        |
| MEHENNI                 | Sylia                    | mehenni.sylia@gmail.com           |

------
### Sujet

« J’suis sapé chez So » est un réseau social de partage de photos, orienté vers l’échange de styles vestimentaires.  
  
Chaque utilisateur est en mesure de poster des photos de lui avec une tenue. Il a la possibilité d'indiquer sur chacune de ses photos le prix et le lieu d'achat de ses vêtements, ainsi que le nom et le modèle. Les photos sont répertoriées par un système de tags, permettant de cibler un certain style, et de proposer des vêtements similaires.

« J’suis sapé chez So » possède une certaine valeur ajoutée par rapport aux solutions déjà existantes. Cette valeur ajoutée est axée sur quatres points :

- Gain de temps : La tenue est déjà prête, il suffit d'un clic pour renouveler toute sa garde robe. Plus besoin de chercher la veste qui ira à votre tenue on a pensé à tout.

- Découvrir de nouveaux styles : Plusieurs styles vestimentaires sont répertoriés, vous pourrez également partager vos tenues et faire découvrir au monde votre propre style grâce au système de tags.

- Economiser de l'argent : Grâce aux partenariats avec différentes boutiques, notre application vous proposera de nombreuses réductions, même en dehors des soldes. De plus, les prix sont affichés, aucune surprise au moment de payer.

- Partenariats : « J’suis sapé chez So » proposera des partenariats entre des grandes marques et les comptes les plus influents de notre plateforme.

------
### Cible

Notre réseau social s'adresse aux utilisateurs de 16 à 34 ans (chiffres Instagram sur 800 millions de personnes). En effet, sur les plateformes de réseaux sociaux spécialisés dans le partage de photos c'est la tranche d'âge la plus active et présente (76%).

J'suis sapé chez So vise aussi à travailler avec de nombreuses marques vestimentaires. Il faut savoir, toujours d'après une étude Instagram, que sur ce genre de réseaux, 90% des top marques mondiales ont un compte professionnel, et pour 80% d'entre eux, au moins une photo par semaine est publiée.

Nous souhaitons également atteindre par la suite les agences de "coach vestimentaire" avec qui nous pourrions élargir les fonctionnalités de notre site et proposer de nouveaux services à nos utilisateurs et plus particulièrement à ceux qui sont à la recherche de relooking.

**Pourquoi cette cible ?**

Tout d'abord il faut savoir qu'entre 16 et 35 ans nous visons majoritairement les profils "étudiants" et "jeunes cadres/salariés" pour qui les problèmes sont de plusieurs natures :

- Pour les uns (étudiants) le manque de revenu qui ne leur permet pas d'aborder le style qu'ils souhaitent à des prix entrant dans leur budget. À l'aide d'une plateforme comme celle de Je suis sapé chez So, les étudiants pourraient fixer à l'aide de nos filtres, une limite de prix et trouver les styles qui leur correspondent.

- Pour les jeunes cadres/salariés, c'est le manque de temps et de recul sur la mode qui leur fait souvent défaut. En effet, il est souvent difficile pour ces profils de trouver le temps de marquer un clivage entre le style qui leur plait et les tenues formelles qu'ils ont l'habitude de porter lors de leur activité professionnelle.

------
### Adresse du Trello

https://trello.com/b/gg6kwFsJ/jsuis-sap%C3%A9-chez-so

------
## Partie libre

### Fonctions principales de git

------
#### Créer un repository vide

    git init
    
------    
#### Cloner un repository existant

    git clone *adresse du repo*
    git clone https://git.iiens.net/cornic2005/pima2016

------
#### Récupérer en local l'état actuel du gitlab

    git pull *adresse du repo*
    git pull http://pedago-etu.ensiie.fr/ChuiSapeChezSo/JeSuisSapeChezSo.git
    
Cette commande est essentielle. Il faut l'utiliser systématiquement:  
- Avant toute modification de votre code en local
- Avant tout push de votre git local

------
#### Envoyer ses commits sur le Gitlab

    git push *adresse du repo*
    git push http://pedago-etu.ensiie.fr/ChuiSapeChezSo/JeSuisSapeChezSo.git

Utiliser cette commande uniquement lorsque vous avez fini quelque chose d'important (fonctionnalité développée/version finale de la correction d'un document)
    
------
#### Afficher les différentes mises à jour du prochain commit

    git status

------
#### Ajouter un fichier dans le prochain commit

    git add *fichier*
    git add README.md
    
------
#### Faire un commit

    git commit
    
------
#### Afficher les logs des précédents commit

    git log
    
------
#### Branches

    git branch *nom_branche*  || Permet de créer une nouvelle branche
    git checkout *nom_branche*  || Permet de se positionner sur une branche
    
    **Pour fusionner brancheB dans brancheA**
    
    git checkout brancheA
    git merge brancheB
    
Normalement on en aura pas spécialement besoin. Elle sert si jamais on décide de développer une même chose de deux manières parce qu'on est pas sûr. Sauf si on veut faire quelque chose de vraiment très très propre avec une branche de développement, une de pré-publication (déboggage) et une qui correspond à la version publiée.
Mais globalement, il faut essayer de s'en servir le moins possible pour éviter les conflits.
    
