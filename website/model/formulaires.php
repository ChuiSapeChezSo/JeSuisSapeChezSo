<?php

echo'
<head>
    <link rel="stylesheet" href="../model/style.css" type="text/css"/>
</head>';

//Formulaires d'inscription pour les utilisateurs 
function form_inscription(){

    
    echo"<form method='post' action='../controler/envoie_inscription.php' onsubmit = 'return verifForm(this)' >";
    
    echo"<fieldset></br>";
    echo"<p>Nom</p><input type='text' name='nom' onblur='verifNom(this)' /><br/>";
    echo"<p>Prenom</p> <input type='text' name='prenom' onblur='verifPrenom(this)'/><br/>";
    echo"<p>Date de naissance</p> <input type='date' name='dob' size='10' placeholder ='AAAA-MM-JJ' '/></br>";
    echo"<p>Pseudo</p> <input type='text' name='pseudo' onblur='verifPseudo(this)' /><br/>";
    echo"<p>Adresse e-mail</p> <input type='text' name='email' onblur='verifMail(this)' /><br/>";
    echo"<p>Mot de passe</p> <input type='password' name='passe' onblur='verifMdp(this)' /><br/>";
    echo"<p>Confirmation du mot de passe</p> <input type='password' name='passe2' onblur='verifMdp(this)' /><br/>";
    echo "</fieldset></br>";
    
    echo "<input class='formButton' type='submit' value='Inscription'/></br>";

}

//Formulaires de connexion utilisateur
function form_connexion(){
    
    echo "<form method='post' action='../controler/envoie_connexion.php'>";
    
    echo"<p>Adresse e-mail</p> <input type='text' name='email'/><br/>";
    echo"<p>Mot de passe</p> <input type='password' name='passe'/><br/>";

    echo "<input class='formButton' type='submit' value='Connexion'/></br>";
    
}


//Formulaire permettant à l'administrateur de supprimer des utilisateurs/recettes
function form_admin(){

    // Créer la page edit_admin avant de décommenter la ligne suivante
    echo"<form method='post' action='../controler/edit_admin.php' >";
    echo"<p>Suppression de l'utilisateur (adresse e-mail)</p><input type='text' name='email' /><br/>";
    echo "<input class='formButton' type='submit' value='Supprimer'/></br>";

} 

// Formulaire d'édition du profil
function form_edit_profil(){


    echo"<form method='post' action='../controler/edit_profil.php'  >";
    
    
    echo"<p>Nouvelle adresse e-mail</p> <input type='text' name='email' onblur = 'verifMail(this)' /><br/>";
    echo"<p>Nouveau pseudo</p> <input type='text' name='pseudo'  onblur = 'verifPseudo(this)'/><br/>";
    echo"<p>Nouveau mot de passe</p> <input type='password' name='passe' onblur = 'verifMdp(this)'/><br/>";
    echo"<p>Confirmation du mot de passe</p> <input type='password' name='passe2' onblur = 'verifMdp(this)' /><br/>";

    echo "<input class='formButton' type='submit' value='Modifier'/></br>";

} 


function upload_image(){

    echo'

    <form method="post" action="../controler/envoie_image.php" enctype="multipart/form-data">
    
     <label for="mon_fichier">Fichier (tous formats | max. 600 Mo) :</label><br />
     <input type="hidden" name="MAX_FILE_SIZE" value="629145600" />
     <input type="file" name="mon_fichier" id="mon_fichier" /><br />

     <label for="description"> Description (max. 140 caractères) :</label><br />
     <textarea name="description" id="description"></textarea><br />

<div class="checkboxes">
    <div class="checkbox"><input type="checkbox" name="tag[]" value="classy">
    <span>Classy</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="urban"> 
    <span>Urban</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="gothique"> 
    <span>Gothique</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="sportwear"> 
    <span>Sportwear</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="casual"> 
    <span>Casual</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="hipster"> 
    <span>Hipster</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="punk"> 
    <span>Punk</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="retro"> 
    <span>Retro</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="pop"> 
    <span>Pop</span></div>

</div>
     <input class="formButton" type="submit" name="submit" value="Envoyer" />
     
    
</form>';

}

function upload_image_profil(){

    echo'

    <form method="post" action="../controler/envoie_image_profil.php" enctype="multipart/form-data">
    
     <label for="mon_fichier">Fichier (tous formats | max. 600 Mo) :</label><br />
     <input type="hidden" name="MAX_FILE_SIZE" value="629145600" />
     <input type="file" name="mon_fichier" id="mon_fichier" /><br />

     <input class="formButton" type="submit" name="submit" value="Envoyer" />
     
    
</form>';


}
function recherche_tag(){

    echo'

    <form method="post" action="../view/page_resultat_recherche.php">
     
  <div class="checkboxes">
    <div class="checkbox"><input type="checkbox" name="tag[]" value="classy">
    <span>Classy</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="urban"> 
    <span>Urban</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="gothique"> 
    <span>Gothique</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="sportwear"> 
    <span>Sportwear</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="casual"> 
    <span>Casual</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="hipster"> 
    <span>Hipster</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="punk"> 
    <span>Punk</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="retro"> 
    <span>Retro</span></div>

    <div class="checkbox">
    <input type="checkbox" name="tag[]" value="pop"> 
    <span>Pop</span></div>
    </div>
     
     <input class="formButton" type="submit" name="submit" value="Envoyer" />
     
</form>';

}
