<?php

    //Permet d'afficher le profil des utilisateurs
    function affiche_profil(){

        //include("../model/database.php");

        // On récupère les infos de l'utilisateur connecté
        $pseudo = $_SESSION['pseudo'];
        $pk_u = $_SESSION['pk_u'];

        $bdd = connect_db();

        // On va chercher toutes les infos de l'utilisateur courant
        $requete = $bdd->query("SELECT * FROM utilisateur WHERE pk_u = '$pk_u'");


        $reponse = $requete->fetch_array();

        // On les récupère
        $nom = $reponse['nom'];
        $prenom = $reponse['prenom'];
        $pseudo = $reponse['pseudo'];
        $email = $reponse['email'];
        $dob = $reponse['dob'];
        $d_inscr = $reponse['d_inscr'];

        // On les affiche
        echo "<fieldset id='infoProfil'>
        <div class='carreHaut'>
        <p>Mes informations</p>
        <li><span class='textLeft'>Nom</span><span class='textRight'>$nom</span></li></br>
        <li><span class='textLeft'>Prenom</span><span class='textRight'>$prenom</span></li></br>
        <li><span class='textLeft'>Pseudo</span><span class='textRight'>$pseudo</span></li></br>
        <li><span class='textLeft'>E-mail</span><span class='textRight'>$email</span></li></br>
        <li><span class='textLeft'>Date de naissance</span><span class='textRight'>$dob</span></li></br>
        <li><span class='textLeft'>Date d'inscription</span><span class='textRight'>$d_inscr</span></li></br>
        </div>
        <div class='carreBas'>
        <p>Gérer mon compte</p>
        
       <a id='editProfil' class='button' href='../view/upload_image_profil.php'> Photo de profil!</a></br>
        <a id='editProfil' class='button' href='../view/page_edit_profil.php'>Edite ton profil !</a></br>
        <a class='button' href = ../view/page_image.php> Upload une image </a> 
        </div>

        </fieldset>
        ";
    }

    function affiche_publi(){

        //include("../model/database.php");

        $pk_u = $_SESSION['pk_u'];
        $bdd = connect_db();

        // On va chercher toutes les infos de l'utilisateur courant
        $requete = $bdd->query("SELECT * FROM images WHERE fk_u = '$pk_u'");

        while($reponse = $requete->fetch_array()){

            $description = $reponse['description'];
            $chemin = $reponse['id_i'];
            $jaime = $reponse['jaime'];
            $note = $reponse['note'];
            $extension = $reponse['extension'];
            $id_i = $reponse['id_i'];

            $chemin = "../database/publi/".$id_i.".".$extension;

            echo "<div class='publication'><img class='imgPublication' src= '$chemin' border='0' /></br> ";
            echo "<div class='textPublication'>".$description.'</br>';
            echo " <div class='avisPublication'>J'aime : ".$jaime."</br>";
            echo " Note : ".$note."</br></div></div></div>";

        }

    }
function affiche_photo_profil(){

    include("../model/database.php");

    $pk_u = $_SESSION['pk_u'];
    $bdd = connect_db();

    // On va chercher toutes les infos de l'utilisateur courant
    $requete = $bdd->query("SELECT * FROM images WHERE fk_u = '$pk_u' AND profil = 1 ");

    while($reponse = $requete->fetch_array()){

        $extension = $reponse['extension'];
        $id_i = $reponse['id_i'];

        $chemin = "../database/publi/".$id_i.".".$extension;

        echo " <div id='photoProfil'><img src= '$chemin' border='0' /></div>";


    }

}

    function affiche_recherche_publi ($image_id) {


        $bdd = connect_db();

        $requete = $bdd->query ("SELECT * FROM images WHERE id_i = '$image_id' ");


        while ($reponse = $requete->fetch_array()) {

            $description = $reponse['description'];
            $chemin = $reponse['id_i'];
            $jaime = $reponse['jaime'];
            $note = $reponse['note'];
            $extension = $reponse['extension'];
            $id_i = $reponse['id_i'];;

            $chemin = "../database/publi/".$id_i.".".$extension;



            echo "<div class='publication'><img class='imgPublication' src= '$chemin' border='0' /></br> ";
            echo "<div class='textPublication'>".$description.'</br>';
            echo " <div class='avisPublication'>J'aime : ".$jaime."</br>";
            echo " Note : ".$note."</br></div></div></div>";


        }

    }

?>