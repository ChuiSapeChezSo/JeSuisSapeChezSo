<?php

session_start();
session_destroy(); // Deconnexion en détruisant la session

header('Location: ../view/index.php'); // Redirection vers la page d'accueil
exit();
?>