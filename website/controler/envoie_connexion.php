<?php

session_start();

//EN ATTENTE DE LA CREATION DE LA BDD POUR POUVOIR MODIFIER TOUT CA


include("../model/database.php");


$bdd = connect_db();

$email = mysqli_real_escape_string($bdd,$_POST['email']);

// On chiffre le mdp à l'aide de md5
$mdp = md5(mysqli_real_escape_string($bdd,$_POST['passe']));

// On récupère les informations associées à l'adresse e-mail renseignée par l'utilisateur
$requete = $bdd->query("SELECT email,password,pseudo,pk_u,admin FROM utilisateur WHERE email = '$email' ");


$reponse = $requete->fetch_array();

// On vérifie que l'adresse e-mail existe dans la BDD
if ($reponse['email'] == NULL){

    //Elle n'existe pas donc on redirige vers la page d'inscription.
    header('Location: ../view/page_inscription.php');
    exit();

}

// Si les informations données par l'utilisateur sont identiques à celles que nous avons récupérés..
// ..on associe celles ci aux variables de sessions

else if ($reponse['email'] == $email && $reponse['password'] == $mdp ){

    $_SESSION['pseudo'] = $reponse['pseudo'];
    $_SESSION['pk_u'] = $reponse['pk_u'];
    $_SESSION['admin'] = $reponse['admin'];

    header('Location: ../view/index.php');
    exit();


}

//Si l'utilisateur s'est trompé de mot de passe, on le renvoie vers un page d'erreur qui lui informe
else {


    header('Location: ../view/erreur_connexion.php');
    exit();


}

?>