-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:8889
-- Généré le :  jeu. 07 déc. 2017 à 16:54
-- Version du serveur :  5.6.35
-- Version de PHP :  7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `jscs`
--

-- --------------------------------------------------------

--
-- Structure de la table `followed`
--

CREATE TABLE `followed` (
  `fk_u` int(11) DEFAULT NULL,
  `followed` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `followers`
--

CREATE TABLE `followers` (
  `fk_u` int(11) DEFAULT NULL,
  `followers` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fk_u` int(11) DEFAULT NULL,
  `id_i` varchar(200) DEFAULT NULL,
  `extension` varchar(10) DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `d_publi` date DEFAULT NULL,
  `note` int(11) DEFAULT NULL,
  `jaime` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `pk_u` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `pseudo` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `dob` date DEFAULT NULL,
  `d_inscr` date DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`pk_u`, `nom`, `prenom`, `pseudo`, `password`, `dob`, `d_inscr`, `email`, `admin`) VALUES
(2, 'Administrateur', 'Admin', 'Admin', '9cdfb439c7876e703e307864c9167a15', '2017-12-07', '2017-12-07', 'admin@admin.fr', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `followed`
--
ALTER TABLE `followed`
  ADD UNIQUE KEY `followed` (`followed`);

--
-- Index pour la table `followers`
--
ALTER TABLE `followers`
  ADD UNIQUE KEY `followers` (`followers`);

--
-- Index pour la table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`pk_u`),
  ADD UNIQUE KEY `pk_u` (`pk_u`),
  ADD UNIQUE KEY `pseudo` (`pseudo`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `email_2` (`email`,`pk_u`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `pk_u` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;