# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Ali Bella Baci*                |
| **Scrum Master**        | *Sami Benelmostafa*                 |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*

> Nous avons tous eu de nombreux projets à rendre dans les autres modules que nous poursuivons, ainsi que des révisions pour les partiels qui se sont avérées imposantes dans nos plannings respectifs.

### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*

> 3 terminés / 7 prévues = 43%

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

> EN TANT QUE So JE VEUX pouvoir taguer une photo que j'uploade AFIN QUE d'autres utilisateurs puissent voir ma photo lorsqu'ils cherchent un style en rapport.
> EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier la photo de profil
> EN TANT QUE So, JE VEUX écrire des tags dans une barre de recherche AFIN DE rechercher un style vestimentaire


## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 

*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*

> Nous avons, à notre sens, choisis avec le PO, la User Story la plus importante vis à vis du principe même de notre site web, à savoir la recherche de style. Nous avons vu lors de notre stand-up meeting que nous n'aurions pas le temps de traiter toutes les User Stories, c'est donc pour cela que nous avons eu ce choix à faire.

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*

> Mettre un accent sur la priorisation des User Stories en prenant en compte les plannings de chacuns pour gagner du temps.

## Prévisions de l'itération suivante  
### Évènements prévus  

*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*

> Les rattrapages de l'inter-semestre peuvent être handicapant dans l'avancée du projet.

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> EN TANT QUE administrateur JE VEUX consulter mon profil AFIN DE supprimer des publications
> EN TANT QUE So, JE VEUX pouvoir taguer le prix de chaque vêtement présent sur la photo AFIN DE permettre aux autres de connaitre le prix général de ma tenue
> EN TANT QUE So, JE VEUX pouvoir taguer un vêtement présent sur la photo AFIN DE pouvoir acheter le même vêtement en utilisant sa référence.


### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> EN TANT QUE So, JE VEUX accéder à une publication AFIN DE pouvoir la commenter
> EN TANT QUE So, JE VEUX accéder à une publication AFIN DE pouvoir l'évaluer

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *2* 	    |  *5* 	|  *0* 	|

> Les ':(' s'expliquent par le planning très compliqué que nous allons devoir gérer.

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *3* 	|  *4* 	|

 
 