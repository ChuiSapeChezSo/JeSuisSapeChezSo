# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Sami Benelmostafa*                |
| **Scrum Master**        | *Ali Bella Baci*                 |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*
> *Nous avons entrepris une démarche de pair programming pour affronter plus efficacement les différentes user story. Nous avons dû modifier les binômes en car les emplois du temps des binômes en place n'étaient pas compatible.  *



### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*

> *3 terminés / 6 prévues = 50%*

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*
> *EN TANT QUE So, JE VEUX uploader une photo AFIN DE partager une tenue.
EN TANT QUE So, JE VEUX accéder à mon profil AFIN DE consulter ses publications.
EN TANT QUE administrateur, JE VEUX consulter un profil AFIN DE supprimer le compte.*

  
## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*
> Partager notre avancement régulièrement notre avancement/nos blocaqges sur une conversation dédiée au projet nous a permis d'organiser des stand-up meeting plus efficacement'

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*

> *Choisir des US en ayant en tête la démo, pour mettre en avant la valeur ajoutée de notre sprint. Soit des avancées très visuelles et non back.
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*
> *PO : Choisir des User Story moins fonctionnelles et plus visuelles.
SM : Présenter en s'appuyant sur des diapositives, et ne pas avoir un discours technique.*

## Prévisions de l'itération suivante  
### Évènements prévus  
*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*
> *Nous entrons en période de campagne BDE, qui peut s'avérer très chronophage étant donné que plusieurs membres de notre groupe doivent organiser les événements autour de cette campagne.*

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*
> *EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter sa photo de profil.
  EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier la photo de profil.
  EN TANT QUE administrateur JE VEUX consulter mon profil AFIN DE supprimer des publications.*



## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *7* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *7* 	|  *0* 	|

 
 
