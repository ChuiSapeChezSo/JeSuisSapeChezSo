# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Mounia Elattaoui*                |
| **Scrum Master**        | *Sylia Mehenni*                 |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*

> Départ de Sylia, notre intégreuse front pour présenter l'école auprès de son ancienne classe préparatoire. Départ de Sofiane à Metz avec l'équipe de football de l'école pour un tournois inter-écoles de France. Obligation artistique d'Ali ce week-end (théâtre).

### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*

> 5 terminés / 7 prévues = 71%

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

> EN TANT QUE So, JE VEUX accéder aux pages du site AFIN D'apprécier la charte graphique du site
> EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter sa photo de profil
> Changer tous les appels à la table "User" en appel à la table "utilisateurs" (correction de bug)
> Regler le bug d'affichage qui bloque le champ d'administration (correction de bug)
> Régler les problèmes d'isset qui autorisent l'accès à l'administration à n'importe qui (correction de bug)


## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 

*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*

> Nous avons réussi à choisir des User Story moins fonctionnelles et plus visuelles, cela s'est ressenti dans la démonstration. Nous avons ajouté un diaporama à notre démo et avons abandonné le discours technique au profit d'un discours plus propice à une démo. Nous continuerons à planifier nos Stand-Up meeting car ils nous aident beaucoup dans l'avancé du projet.

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*

> Faire attention au planning car par mégarde nous avons choisis un Scrum Master qui ne pouvait pas être là pour la démo

## Prévisions de l'itération suivante  
### Évènements prévus  

*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*

> La révision de l'ensemble des partiels, les périodes de fêtes et la fin de la campagne BDE risquent de compliquer le planning, nous allons devoir être vigilents pour attendre nos objectifs.


### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier la photo de profil
> EN TANT QUE administrateur JE VEUX consulter mon profil AFIN DE supprimer des publications

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> EN TANT QUE So, JE VEUX pouvoir taguer le prix de chaque vêtement présent sur la photo AFIN DE permettre aux autres de connaitre le prix général de ma tenue

> EN TANT QUE So, JE VEUX pouvoir taguer un vêtement présent sur la photo AFIN DE pouvoir acheter le même vêtement en utilisant sa référence.

> EN TANT QUE So, JE VEUX accéder à une publication AFIN DE pouvoir la commenter

> EN TANT QUE So, JE VEUX accéder à une publication AFIN DE pouvoir l'évaluer

> EN TANT QUE So JE VEUX pouvoir taguer une photo que j'uploade AFIN QUE d'autres utilisateurs puissent voir ma photo lorsqu'ils cherchent un style en rapport.

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *2* 	|  5* 	    |  *0* 	|  *0* 	|

> Les ':(' s'expliquent par le planning très compliqué que nous allons devoir gérer.

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 7 	|  *0* 	|  *0* 	    |  *3* 	|  *4* 	|

 
 