# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Amina Zeghida*                |
| **Scrum Master**        | *Sofiane Kihal*                 |


## Bilan de l'itération précédente  
### Évènements 

*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*

> Difficultés lors de la création et la gestion du Git. On a également eu du mal à comprendre exactement ce qu'on attendait des User Story et ce qui devait être présent sur le Trello ou non. 

Nous nous sommes réunis autour d'une réunion le lundi 09/10/2017 pour définir les axes et objectifs de notre sujet et de notre projet. Cela nous a permis de définir ce qui est présent dans le README du trello (que l'on va push sur le Git)


### Taux de complétion de l'itération  
*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*

>  7 terminés / 9 prévues = 78%*

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*

> Ce ne sont pas des users story, mais nous avons : 

> - Configuré notre Trello
> - Configuré notre Git
> - Choisi un IDE (que l'on doit changer)
> - Défini les tenants et aboutissants du projet autour d'un README (sur le Trello et prochainement sur le git ), à savoir : qu'est-ce que nous souhaitons faire ? Quelle est notre cible? Quelle est la valeur ajoutée de notre service/produit par rapport aux autres?

## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*

> C'est notre premiere itération, donc pas de rétrospective sur l'itération précédente (car elle n'existe pas).

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*

Prioriser rapidement les User Stories pour entrer dan le sprint planning et commencer à développer les premieres fonctionnalités.
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*

> Améliorer la communication entre Scrum Master PO et le client pour mieux fixer les objctifs du sprint à l'avenir.

## Prévisions de l'itération suivante  
### Évènements prévus  

*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*

> La semaine de soutenance pourrait légèrement ralentir l'itération.  

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> Aucune user story, mais il faut que l'on choisisse un nouvel IDE rapidement.

### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> Les User stories qui suivent ne sont pas dans un ordre priorisé : 

> 1) EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier le mot de passe
> 2) EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier le pseudo
> 3) EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier la photo de profil
> 4) EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter ses publications
> 5) EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter son pseudo
> 6) EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter sa photo de profil
> 7) EN TANT QUE So, JE VEUX uploader une photo AFIN DE partager une tenue.
> 8) EN TANT QUE So, JE VEUX cliquer sur un tag associé à une publication AFIN DE visualiser des styles similaires
> 9) EN TANT QUE So JE VEUX pouvoir taguer une photo que j'uploade AFIN QUE d'autres utilisateurs puissent voir ma photo lorsqu'ils cherchent un style en rapport.
> 10) EN TANT QUE So, JE VEUX écrire des tags (de style, de marque) dans une barre de recherche AFIN DE composer mon style vestimentaire

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *7* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *5* 	|  *2* 	|

 
 