# Rapport d'itération  
*Ce rapport est à fournir pour chaque équipe au moment du changement d'itération.*

## Composition de l'équipe 
*Remplir ce tableau avec la composition de l'équipe et les rôles.*

|  &nbsp;                 | Itération précédente     |
| -------------           |-------------             |
| **Product Owner**       | *Sofiane Kihal*                |
| **Scrum Master**        | *Mounia Elattaoui*                 |

## Bilan de l'itération précédente  
### Évènements 
*Quels sont les évènements qui ont marqué l'itération précédente? Répertoriez ici les évènements qui ont eu un impact sur ce qui était prévu à l'itération précédente.*

> La semaine de soutenance et des entreprises a freiné notre avancement. De plus, s'en est suivi une semaine de controle durant laquelle nous n'avons pas pu travailler. Nous avons donc profité de la semaine de repos pour réaliser un maximum de tâches.

### Taux de complétion de l'itération  

*Quel est le nombre d'éléments terminés par rapport au nombre total d'éléments prévu pour l'itération?*

> 5 terminés / 10 prévues = 50%

### Liste des User Stories terminées
*Quelles sont les User Stories qui ont été validées par le PO à la fin de l'itération ?*


> 1) EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier le mot de passe
> 2) EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier le pseudo
> 3) EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter ses publications
> 4) EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter son pseudo
> 5) EN TANT QUE So, JE VEUX accéder un profil AFIN DE consulter sa photo de profil


## Rétrospective de l'itération précédente
  
### Bilans des retours et des précédentes actions 
*Quels sont les retours faits par l'équipe pendant la rétrospective? Quelles sont les actions qui ont apporté quelque chose ou non?*

> Nous avons été trop ambitieux dans la répartition des User Stories sur la semaine d'itération. Nous n'avons pas pris en compte l'impact que les évènements pouvaient avoir sur le projet, ni la première prise en main des outils tels que le git ou l'IDE qui nous ont également freiné dans notre avancé. Dorénavant, ce ne sera plus un problème puisque la prise en main est effective.

> Nous avons pu nous retrouver avant la semaine de pause afin d'effectuer un stand-up meeting qui nous a permis de recadrer un peu notre projet sur les questions d'utilisations du Git et de l'IDE. Cependant, l'avancement n'était pas assez conséquent pour pouvoir transmettre un rapport d'itération pertinent à ce moment là.

### Actions prises pour la prochaine itération
*Quelles sont les actions décidées par l'équipe pour la prochaine itération ?*

> Nous souhaitons réaliser les User Stories que nous n'avons pas pu réaliser à l'itération précédente, puis mettre en place un serveur pour que quiconque puisse visualiser le site web sans avoir à executer le code.
 
### Axes d'améliorations 
*Quels sont les axes d'améliorations pour les personnes qui ont tenu les rôles de PO, SM et Dev sur la précédente itération?*

> Avoir une répartition et une pondération du temps sur les User Stories plus réaliste. L'utilisation du trello a été respectée. Sur l'utilisation du Git, il faut dorénavant afficher des commit uniquement sur les fichiers modifiés, en effet, parfois nous commitions sur tous les fichiers alors que nous n'avions modifié qu'un seul.

## Prévisions de l'itération suivante  
### Évènements prévus  

*Quels sont les évènements qui vont peut être impacter l'itération? Répertoriez ici les évènements que vous anticipez comme ayant un impact potentiel pour l'itération (absences, changement de cap, difficultés, etc.).*

> Nous allons entrer dans notre 2ème semaine de contrôles continus, puis, normalement, nous devrions avoir du temps libre pour enchaîner notre itération.

### Titre des User Stories reportées  
*Lister ici les éléments des itérations précédentes qui ont été reportés à l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> 1) EN TANT QUE So, JE VEUX consulter mon profil AFIN DE le modifier la photo de profil
> 2) EN TANT QUE So, JE VEUX uploader une photo AFIN DE partager une tenue.
> 3) EN TANT QUE So, JE VEUX cliquer sur un tag associé à une publication AFIN DE visualiser des styles similaires
> 4) EN TANT QUE So JE VEUX pouvoir taguer une photo que j'uploade AFIN QUE d'autres utilisateurs puissent voir ma photo lorsqu'ils cherchent un style en rapport.
> 5) EN TANT QUE So, JE VEUX écrire des tags (de style, de marque) dans une barre de recherche AFIN DE composer mon style vestimentaire


### Titre des nouvelles User Stories  
*Lister ici les nouveaux éléments pour l'itération suivante. Ces éléments ont dû être revus et corrigés par le PO.*

> EN TANT QUE administrateur, JE VEUX consulter un profil AFIN DE supprimer le compte
> EN TANT QUE administrateur JE VEUX consulter un profil AFIN DE supprimer des publications

## Confiance 
### Taux de confiance de l'équipe dans l'itération  
*Remplir le tableau sachant que :D est une confiance totale dans le fait de livrer les éléments de l'itération. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *7* 	|  *0* 	|

### Taux de confiance de l'équipe pour la réalisation du projet 
*Remplir le tableau sachant que :D est une confiance totale dans le fait de réaliser le projet. Mettre le nombre de votes dans chacune des cases. Expliquer en cas de besoin.*

|          	| :( 	| :&#124; 	| :) 	| :D 	|
|:--------:	|:----:	|:----:	    |:----:	|:----:	|
| Equipe 17 	|  *0* 	|  *0* 	    |  *7* 	|  *0* 	|

 
 